#!/bin/bash

BATS_TESTS_DIR=test/bats/tests
POLICY_DIR=policies
WAIT_TIME=15
SLEEP_TIME=1

kubectl delete -f ${BATS_TESTS_DIR}/good/no_dupe_ns.yaml
kubectl delete -f ${BATS_TESTS_DIR}/good/good_ns.yaml
kubectl delete -f ${BATS_TESTS_DIR}/bad/bad_ns.yaml
kubectl delete -f ${BATS_TESTS_DIR}/bad/opa_no_limits.yaml
kubectl delete -f ${BATS_TESTS_DIR}/good/opa.yaml
kubectl delete -f ${BATS_TESTS_DIR}/bad/no_dupe_ns_2.yaml