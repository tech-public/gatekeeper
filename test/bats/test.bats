#!/usr/bin/env bats

load helpers

BATS_TESTS_DIR=test/bats/tests
POLICY_DIR=policies
WAIT_TIME=120
SLEEP_TIME=1

@test "gatekeeper-controller-manager is running" {
  run wait_for_process $WAIT_TIME $SLEEP_TIME "kubectl -n gatekeeper-system wait --for=condition=Ready --timeout=10s pod -l control-plane=controller-manager"
  assert_success
}

@test "constrainttemplates crd is established" {
  wait_for_process $WAIT_TIME $SLEEP_TIME "kubectl -n gatekeeper-system wait --for condition=established --timeout=60s crd/constrainttemplates.templates.gatekeeper.sh"

  run kubectl -n gatekeeper-system get crd/constrainttemplates.templates.gatekeeper.sh
  assert_success
}

@test "waiting for validating webhook" {
	wait_for_process $WAIT_TIME $SLEEP_TIME "kubectl get validatingwebhookconfigurations.admissionregistration.k8s.io gatekeeper-validating-webhook-configuration"

  run kubectl get validatingwebhookconfigurations.admissionregistration.k8s.io gatekeeper-validating-webhook-configuration
  assert_success
}

@test "applying sync config" {
  run kubectl apply -f ${BATS_TESTS_DIR}/sync.yaml
  assert_success
}

@test "required labels test" {
  run kubectl apply -f ${POLICY_DIR}/templates/k8srequiredlabels_template.yaml -n gatekeeper-system
  assert_success

	wait_for_process $WAIT_TIME $SLEEP_TIME "kubectl -n gatekeeper-system wait --for condition=established --timeout=60s crd/k8srequiredlabels.constraints.gatekeeper.sh"

  run kubectl apply -f ${POLICY_DIR}/constraints/all_pod_must_have_gk.yaml
  assert_success

  wait_for_process $WAIT_TIME $SLEEP_TIME "kubectl get k8srequiredlabels.constraints.gatekeeper.sh pod-must-have-gk-apis -o yaml | grep enforced"
}

@test "container limits test" {
  run kubectl apply -f ${POLICY_DIR}/templates/k8scontainterlimits_template.yaml -n gatekeeper-system
  assert_success

  wait_for_process $WAIT_TIME $SLEEP_TIME "kubectl -n gatekeeper-system wait --for condition=established --timeout=60s crd/k8scontainerlimits.constraints.gatekeeper.sh"

  run kubectl get crd/k8scontainerlimits.constraints.gatekeeper.sh
  assert_success

  run kubectl apply -f ${POLICY_DIR}/constraints/containers_must_be_limited.yaml -n gatekeeper-system
  assert_match 'k8scontainerlimits.constraints.gatekeeper.sh/container-must-have-limits-apis unchanged' "$output"
  assert_success

  wait_for_process $WAIT_TIME $SLEEP_TIME "kubectl get k8scontainerlimits.constraints.gatekeeper.sh container-must-have-limits-apis -o yaml | grep enforced"

  run kubectl apply -f ${BATS_TESTS_DIR}/bad/opa_no_limits.yaml
  assert_match 'denied the request' "$output"
  assert_failure

  run kubectl apply -f ${BATS_TESTS_DIR}/good/opa.yaml
  assert_success
}

@test "required labels audit test for API Pods" {
  wait_for_process $WAIT_TIME $SLEEP_TIME "kubectl get k8srequiredlabels.constraints.gatekeeper.sh ns-must-have-gk -o json | jq '.status.violations[]'"

  violations=$(kubectl get k8srequiredlabels.constraints.gatekeeper.sh pod-must-have-gk-apis -o json | jq '.status.violations | length')
  [[ "$violations" -eq 0 ]]

  totalViolations=$(kubectl get k8srequiredlabels.constraints.gatekeeper.sh pod-must-have-gk-apis -o json | jq '.status.totalViolations')
  [[ "$totalViolations" -eq 0 ]]
}

@test "Pod Limit audit test for the apis" {
  wait_for_process $WAIT_TIME $SLEEP_TIME "kubectl get k8scontainerlimits.constraints.gatekeeper.sh/container-must-have-limits -o json | jq '.status.violations[]'"

  violations=$(kubectl get k8srequiredlabels.constraints.gatekeeper.sh pod-must-have-gk -o json | jq '.status.violations | length')
  [[ "$violations" -eq 0 ]]

  totalViolations=$(kubectl get k8srequiredlabels.constraints.gatekeeper.sh pod-must-have-gk -o json | jq '.status.totalViolations')
  [[ "$totalViolations" -eq 0 ]]
}

@test "Whitelisted API Images audit test" {
  wait_for_process $WAIT_TIME $SLEEP_TIME "kubectl get k8sallowedrepos.constraints.gatekeeper.sh/whitelisted-repos-apis -o json | jq '.status.violations[]'"

  violations=$(kubectl get kubectl get k8sallowedrepos.constraints.gatekeeper.sh/whitelisted-repos-apis -o json | jq '.status.violations | length')
  [[ "$violations" -eq 0 ]]

  totalViolations=$(kubectl get k8sallowedrepos.constraints.gatekeeper.sh/whitelisted-repos-apis -o json | jq '.status.totalViolations')
  [[ "$totalViolations" -eq 0 ]]
}

@test "Whitelisted other cluster Images audit test" {
  wait_for_process $WAIT_TIME $SLEEP_TIME "kubectl get k8sallowedrepos.constraints.gatekeeper.sh/whitelisted-repos-others -o json | jq '.status.violations[]'"

  violations=$(kubectl get k8sallowedrepos.constraints.gatekeeper.sh/whitelisted-repos-others -o json | jq '.status.violations | length')
  [[ "$violations" -eq 0 ]]

  totalViolations=$(kubectl get k8sallowedrepos.constraints.gatekeeper.sh/whitelisted-repos-others -o json | jq '.status.totalViolations')
  [[ "$totalViolations" -eq 0 ]]
}

