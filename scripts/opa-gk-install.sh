#!/bin/bash

set -eo pipefail
echo "Present working directory" : `pwd`
echo "--- Installing OPA Gatekeeper ---"
helm upgrade --install opa chart/gatekeeper-operator/