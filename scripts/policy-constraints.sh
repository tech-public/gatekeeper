#!/bin/bash

set -eo pipefail

kubectl apply -f ./policies/constraints/all_pod_must_have_gk.yaml -n gatekeeper-system
kubectl apply -f ./policies/constraints/whitelisted-repos-apis.yaml -n gatekeeper-system
kubectl apply -f ./policies/constraints/whitelisted-repos-others.yaml -n gatekeeper-system
kubectl apply -f ./policies/constraints/containers_must_be_limited.yaml -n gatekeeper-system