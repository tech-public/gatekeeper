#!/bin/bash -e

# don't forget to source me so the KUBECONFIG var is exported!
# . ./scripts/get-kubeconfig.sh

export AWS_ACCOUNT=`aws sts get-caller-identity --output text --query 'Account'`
aws s3 cp s3://${CLUSTER_NAME}-${AWS_ACCOUNT}-${AWS_REGION}/kubeconfig.${CLUSTER_NAME}.yaml .
export KUBECONFIG=./kubeconfig.${CLUSTER_NAME}.yaml
echo "KUBECONFIG=${KUBECONFIG}"