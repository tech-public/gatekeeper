#!/bin/bash

set -eo pipefail

kubectl apply -f ./policies/templates/k8sallowedrepos_template.yaml -n gatekeeper-system
kubectl apply -f ./policies/templates/k8srequiredlabels_template.yaml -n gatekeeper-system
kubectl apply -f ./policies/templates/k8scontainterlimits_template.yaml -n gatekeeper-system