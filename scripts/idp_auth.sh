#!/bin/bash

set +e

echo "working directory: `pwd`"

echo --- creating ~/.aws/config ---
mkdir -p ~/.aws
chmod 755 ~/.aws

echo -e '[profile saml]
region = ap-southeast-2' > ~/.aws/config

echo --- configuring saml2aws ---
echo "AWS_PROFILE: ${AWS_PROFILE}"
saml2aws configure --url ${CI_ENVIRONMENT_URL} \
                   --idp-account ${AWS_PROFILE} \
                   --idp-provider KeyCloak \
                   --username ${IDP_USER} \
                   --mfa Auto \
                   --skip-prompt

echo --- authenticating using saml2aws ---
saml2aws login --profile=${AWS_PROFILE} --idp-account=${AWS_PROFILE} --password=${PASSWORD} --skip-prompt --force

echo --- setting AWS_DEFAULT_PROFILE ---
export AWS_DEFAULT_PROFILE=${AWS_PROFILE}

echo --- authenticated with IAM role: ---
aws sts get-caller-identity