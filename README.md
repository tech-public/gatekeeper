# Gatekeeper

[![Build Status](https://travis-ci.org/open-policy-agent/gatekeeper.svg?branch=master)](https://travis-ci.org/open-policy-agent/gatekeeper) [![Docker Repository on Quay](https://quay.io/repository/open-policy-agent/gatekeeper/status "Docker Repository on Quay")](https://quay.io/repository/open-policy-agent/gatekeeper


## Goals

Every organization has policies. Some are essential to meet governance and legal requirements. Others help ensure adherance to best practices and institutional conventions. Attempting to ensure compliance manually would be error-prone and frustrating. Automating policy enforcement ensures consistency, lowers development latency through immediate feedback, and helps with agility by allowing developers to operate independently without sacrificing compliance.

Kubernetes allows decoupling policy decisions from the inner workings of the API Server by means of [admission controller webhooks](https://kubernetes.io/docs/reference/access-authn-authz/extensible-admission-controllers/), which are executed whenever a resource is created, updated or deleted. Gatekeeper is a validating (mutating TBA) webhook that enforces CRD-based policies.

In addition to the `admission` scenario, Gatekeeper's audit functionality allows administrators to see what resources are currently violating any given policy.

## Prerequisites

### Minimum Kubernetes Version

**To use Gatekeeper, you should have a minimum Kubernetes version of 1.14, which adds
webhook timeouts.**

### RBAC Permissions

For the installation, make sure you have cluster admin permissions: 

```sh
  kubectl create clusterrolebinding cluster-admin-binding \
    --clusterrole cluster-admin \
    --user <YOUR USER NAME>

NOTE: This project is using an admin user configured in the hidden and secure section and is not visible to Public. 
AWS Assume role API is used to make required API calls. Refer : Repo/gatekeeper/scripts/idp_auth.sh
```
## Installation:
```
.... is automated and scripts can be found at Repo/gatekeeper/scripts/opa-gk-install.sh

The installation method is through helm and the charts are located at Repo/gatekeeper/chart
```

## Policies
```
There are the below policies defined as of date

1. Ensuring all the pods in the istio enabled namespace have the gatekeeper label.
refer : Repo/opa-gatekeeper/policies/constraints/all_pod_must_have_gk.yaml

2. Ensuring all the API containers have the resource limits on them.
Refer: Repo/opa-gatekeeper/policies/constraints/containers_must_be_limited.yaml

3. Ensuring only specific repos are whitelisted. Pods outside the whitelisted repos, will not be allowed to run. It is necessary that the policy is implemented prior the pod creation else the policy will only audit the violation but not prevent the pod from creation.
Refer: Repo/opa-gatekeeper/policies/constraints/whitelisted-repos-apis.yaml
and
Repo/opa-gatekeeper/policies/constraints/whitelisted-repos-others.yaml

```

## Templates
```
Policy Templates are defined at the below path
Repo/opa-gatekeeper/policies/templates
```

## Constraints
```
All the policy constraints are defined at the below path.
Repo/opa-gatekeeper/policies/constraints
```

## How to Whitelist a new Repo
```
Add the Repo in the below file
Repo/opa-gatekeeper/policies/constraints/whitelisted-repos-others.yaml
```

## How can I see the policy violations
```
Run the below similar command for each policy

kubectl get k8sallowedrepos.constraints.gatekeeper.sh/whitelisted-repos-others -o json | jq '.status.violations'

Sample Output:

[
  {
    "enforcementAction": "deny",
    "kind": "Pod",
    "message": "container <istio-init-crd-11> has an invalid image repo <docker.io/istio/kubectl:1.3.6>, allowed repos are [\"registry.gitlab.com/flybuys/app/\", \"docker.io/istio/proxy_init\", \"docker.io/istio/proxyv2\", \"docker.io/istio/citadel\", \"docker.io/istio/galley\",...",
    "name": "istio-init-crd-11-1.3.6-xwvh6",
    "namespace": "istio-system"
  },
  {
    "enforcementAction": "deny",
    "kind": "Pod",
    "message": "container <istio-init-crd-10> has an invalid image repo <docker.io/istio/kubectl:1.3.6>, allowed repos are [\"registry.gitlab.com/flybuys/app/\", \"docker.io/istio/proxy_init\", \"docker.io/istio/proxyv2\", \"docker.io/istio/citadel\", \"docker.io/istio/galley\",...",
    "name": "istio-init-crd-10-1.3.6-8gdrv",
    "namespace": "istio-system"
  },
  {
    "enforcementAction": "deny",
    "kind": "Pod",
    "message": "container <istio-init-crd-12> has an invalid image repo <docker.io/istio/kubectl:1.3.6>, allowed repos are [\"registry.gitlab.com/flybuys/app/\", \"docker.io/istio/proxy_init\", \"docker.io/istio/proxyv2\", \"docker.io/istio/citadel\", \"docker.io/istio/galley\",...",
    "name": "istio-init-crd-12-1.3.6-5c6dc",
    "namespace": "istio-system"
  },
```
## Pipeline trigger
```
Pipeline can be trigerred with the below variables depending upon the environment the deployment needs to be trigerred.

**For Dev:**
ACTION=deploy_dev

**For Test:**
ACTION=deploy_test

**For Production:**
ACTION=deploy_prod
```

## Test Automation
```
Test cases are written in bats and can be found at Repo/gatekeeper/test





